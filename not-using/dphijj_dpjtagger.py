# Program to count number of events in each ABCD region
# Plots ABCD plane for signal and data
# Returns yields

from ROOT import TH2D, TLine, TCanvas, TTree, TFile, kRed, TLatex, TPaveText, gPad, gStyle
import ctypes
import math
import numpy as np


def main(ttree, infile, cut, mode):

  # Set default vars for nA estimation
  # isoID
  xbins = 19
  xmin = 0.5
  xmax = 1
  xcut = 0.95

  # |dphijj|
  ybins = 14
  ymax = 3.2
  ycut = 2.5 
  ymin = 0
  
  fullhadronic_presel = "(scale1fb*intLumi)*(njet30>1&&mjj>1e6&&abs(detajj)>3&&hadDPJTrig==1&&(MET>80e3&&MET<225e3)&&(nLJjets20>0&&nLJmus20==0)&&(neleSignal==0&&nmuSignal==0&&hasBjet==0)&&min_dphi_jetmet>0.4&&LJjet1_gapRatio>0.9&&LJjet1_BIBtagger>0.2&&LJjet1_jvt<0.4)" 
  validation_presel = "(scale1fb*intLumi)*(njet30>1&&mjj>1e6&&abs(detajj)>3&&hadDPJTrig==1&&(MET>80e3&&MET<225e3)&&(nLJjets20>0&&nLJmus20==0)&&(neleSignal==0&&nmuSignal==0&&hasBjet==0)&&min_dphi_jetmet<0.4&&LJjet1_gapRatio>0.9&&LJjet1_BIBtagger>0.2&&LJjet1_jvt<0.4)" 
  lowStat_presel = "(scale1fb*intLumi)*(njet30>1&&mjj>1e6&&abs(detajj)>3&&MET>80e3&&MET<225e3&&nLJjets20>0&&nLJmus20==0&&neleSignal==0&&nmuSignal==0&&hasBjet==0&&min_dphi_jetmet>0.4&&LJjet1_gapRatio>0.9)" 
  presel = fullhadronic_presel
  
  if (mode == "estimate" and "data" in infile and presel == fullhadronic_presel): presel += "&&(LJjet1_DPJtagger<0.95||abs(dphijj)>2.5)" 
  #if mode == "LJjet1DPJtagger":
  if mode == "dphijj": ycut = float(cut) 
  
  # get nevents in each region
  hA = TH2D("hA", "hA", xbins, xcut, xmax, ybins, ymin, ycut)
  hA.Sumw2()
  hB = TH2D("hB", "hB", xbins, xcut, xmax, ybins, ycut, ymax)
  hB.Sumw2()
  hC = TH2D("hC", "hC", xbins, xcut, xmax, ybins, ycut, ymax)
  hC.Sumw2()
  hD = TH2D("hD", "hD", xbins, xmin, xcut, ybins, ymin, ycut)
  hD.Sumw2()
  h = TH2D("h_"+infile, "LJjet1_DPJtagger:abs(dphijj) ", xbins, xmin, xmax, ybins, ymin, ymax)
  h.Sumw2()

  print("xmin, xcut, xmax: ", xmin, xcut, xmax)
  print("ymin, ycut, ymax: ", ymin, ycut, ymax)
  # define regional cuts
  cutA = "(LJjet1_DPJtagger>="+str(xcut)+"&&LJjet1_DPJtagger<="+str(xmax)+"&&abs(dphijj)>="+str(ymin)+"&&abs(dphijj)<"+str(ycut)+")"
  cutB = "(LJjet1_DPJtagger>="+str(xcut)+"&&LJjet1_DPJtagger<="+str(xmax)+"&&abs(dphijj)>="+str(ycut)+"&&abs(dphijj)<="+str(ymax)+")"
  cutC = "(LJjet1_DPJtagger>="+str(xmin)+"&&LJjet1_DPJtagger<"+str(xcut)+"&&abs(dphijj)>="+str(ycut)+"&&abs(dphijj)<="+str(ymax)+")"
  cutD = "(LJjet1_DPJtagger>="+str(xmin)+"&&LJjet1_DPJtagger<"+str(xcut)+"&&abs(dphijj)>="+str(ymin)+"&&abs(dphijj)<"+str(ycut)+")"
  cutABCD = "(LJjet1_DPJtagger>="+str(xmin)+"&&LJjet1_DPJtagger<="+str(xmax)+"&&abs(dphijj)>="+str(ymin)+"&&abs(dphijj)<="+str(ymax)+")"

  # errors for TH1 integral
  errA = ctypes.c_double(0)
  errB = ctypes.c_double(0)
  errC = ctypes.c_double(0)
  errD = ctypes.c_double(0)
  errN = ctypes.c_double(0)
  
  # create histogram and extract nevents for each region ABCD
  canvas = TCanvas("c", "c", 10, 10, 800, 500)
  canvas.cd()
  ttree.Draw("abs(dphijj):LJjet1_DPJtagger>>hA", (presel+"*"+cutA), "colz")
  nA = hA.IntegralAndError(0, xbins+1, 0, ybins+1, errA)
  
  canvas.Clear()
  canvas.cd()
  ttree.Draw("abs(dphijj):LJjet1_DPJtagger>>hB", (presel+"*"+cutB), "colz")
  nB = hB.IntegralAndError(0, xbins+1, 0, ybins+1, errB) 
  canvas.Clear()
  
  canvas.cd()
  ttree.Draw("abs(dphijj):LJjet1_DPJtagger>>hC", (presel+"*"+cutC), "colz")
  nC = hC.IntegralAndError(0, xbins+1, 0, ybins+1, errC)
  canvas.Clear()

  canvas.cd()
  ttree.Draw("abs(dphijj):LJjet1_DPJtagger>>hD", (presel+"*"+cutD), "colz")
  nD = hD.IntegralAndError(0, xbins+1, 0, ybins+1, errD)
  canvas.Clear()

  canvas.cd()
  
  if "qcd" in infile: title = "\\mbox{QCD: LJjet1 DPJtagger vs. }\mathrm{|\Delta\phi_{jj}|}"
  if "data" in infile: title = "\\mbox{Full Run 2 Data: LJjet1 DPJtagger vs.}\mathrm{|\Delta\phi_{jj}|}"
  if "vbf" in infile: title = "\\mbox{"+str(infile.replace("frvz_", "").replace("_", " "))+": LJjet1 DPJtagger vs.}\mathrm{|\Delta\phi_{jj}|}"

  # draw histogram of total ABCD plane and save to file
  ttree.Draw("abs(dphijj):LJjet1_DPJtagger>>h_"+infile, (presel+"*"+cutABCD), "colz")
  n = h.IntegralAndError(0, xbins+1, 0, ybins+1, errN)
  gPad.Update()
  h.SetNameTitle(infile, title)
  h.SetXTitle("\mathrm{LJjet1\;DPJtagger}")
  h.SetYTitle("\mathrm{|\Delta\phi_{jj}|}")
  h.GetXaxis().SetRange(0, xbins)
  h.GetYaxis().SetRange(0, ybins)
  stats = h.FindObject("stats")
  stats.SetX1NDC(0.2)
  stats.SetX2NDC(0.4)
  stats.SetY1NDC(0.2)
  stats.SetY2NDC(0.4)
  #gStyle.SetOptStat(0)
  print("ABCD plane correlation: ", round(h.GetCorrelationFactor(1, 2), 4)) 
  '''A = TPaveText(0.96,2.1,0.985,2.4);
  B = TPaveText(0.96,2.6,0.985,2.9);
  C = TPaveText(0.915,2.6,0.94,2.9);
  D = TPaveText(0.915,2.1,0.94,2.4);'''
  A = TPaveText(0.955,2.1,0.995,2.4);
  B = TPaveText(0.955,2.6,0.995,2.9);
  C = TPaveText(0.9,2.6,0.94,2.9);
  D = TPaveText(0.9,2.1,0.94,2.4);
  A.SetBorderSize(1)
  B.SetBorderSize(1)
  C.SetBorderSize(1)
  D.SetBorderSize(1)
  A.SetTextSize(0.06);
  B.SetTextSize(0.06);
  C.SetTextSize(0.06);
  D.SetTextSize(0.06);
  A.SetFillColor(0);
  B.SetFillColor(0);
  C.SetFillColor(0);
  D.SetFillColor(0);
  A.SetTextAlign(12);
  B.SetTextAlign(12);
  C.SetTextAlign(12);
  D.SetTextAlign(12);
  A.AddText("A");
  B.AddText("B");
  C.AddText("C");
  D.AddText("D");
  A.Draw();
  B.Draw();
  C.Draw();
  D.Draw();
  ycut = TLine(xmin, ycut, xmax, ycut)
  ycut.SetLineColor(kRed)
  ycut.SetLineWidth(2)
  ycut.Draw("same")
  xcut = TLine(xcut, ymin, xcut, ymax)
  xcut.SetLineColor(kRed)
  xcut.SetLineWidth(2)
  xcut.Draw("same")
  canvas.SetLogz()
  canvas.Print("/Users/s1891044/Documents/DarkPhotons/vbf-hdpj-abcd-framework/output/detajj_isoID/" + infile + "_" + cut + ".png")
  print("Plot saved to output/detajj_isoID/" + infile + "_" + cut + ".png")
  # return nevents in each region + stat. error
  vals = [nA, nB, nC, nD, n, errA.value, errB.value, errC.value, errD.value, errN.value]
  
  return vals

