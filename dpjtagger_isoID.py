# Program to count number of events in each ABCD region
# Plots ABCD plane for signal and data
# Returns yields

from ROOT import TH2D, TLine, TCanvas, TTree, TFile, kRed, TLatex, TPaveText, gPad, gStyle
import ctypes
import math
import numpy as np

def main(ttree, infile, mode, cuts):

  # Set default vars for nA estimation
  # isoID
  xbins = 80
  xmin = 0 
  xcut = cuts[0]
  xmax = 20

  # dpjtagger 
  ybins = 80 
  ymin = 0.2 #2.5
  ycut = cuts[1]
  ymax = 1

  weights = "(scale1fb*intLumi)"
  vbfFilter = "(njet30>1&&mjj>1e6&&abs(detajj)>3)"
  trigger = "(hadDPJTrig==1||metTrig==1)" 
  lowMET_trigger = "(hadDPJTrig==1)" 
  minMET = "(MET>80e3)" 
  lowMET = "(MET>80e3&&MET<225e3)" 
  highMET = "(MET>225e3)"
  dpjContent = "(nLJjets20>0&&nLJmus20==0)"
  leptonVeto = "(neleSignal==0&&nmuSignal==0)"
  bjetVeto = "(hasBjet==0)"
  BIBremoval = "(LJjet1_BIBtagger>0.2)"
  dpjQuality = "(LJjet1_gapRatio>0.9&&LJjet1_jvt<0.4)" 
  dphijj = "(abs(dphijj)<2.5)"
  invert_jvt = "(LJjet1_jvt>0.4)"
  jetmet = "(min_dphi_jetmet>0.4)"
  invert_jetmet = "(min_dphi_jetmet<0.4)"
  centrality = "(LJjet1_centrality>0.7)"
  # validation
  invert_dphijj = "(abs(dphijj)>2.5)"
  invert_centrality = "(LJjet1_centrality<0.5)"
  BC = "()"
  
  #qcd_presel = weights+"*("+vbfFilter+"&&"+trigger+"&&"+minMET
  fullhadronic_presel = weights+"*("+vbfFilter+"&&"+lowMET_trigger+"&&"+lowMET+"&&"+dpjContent+"&&"+leptonVeto+"&&"+bjetVeto+"&&"+dpjQuality+"&&"+BIBremoval+"&&"+jetmet+"&&"+dphijj

  if "estimation" in mode: presel = fullhadronic_presel 
  if "correlation" in mode: presel, xmin, xbins = fullhadronic_presel, 4.5, 31 
  if "control-region" in mode: presel = dphijjCR_presel 
  if "sub-region" in mode: presel, ymin, ybins = BC_presel, 1, 4 
  
   
  # blinding cut
  if "Data" in infile: presel += "&&(LJjet1_DPJtagger<"+str(ycut)+"||LJjet1_isoID*0.001>="+str(xcut)+")"
  
  # get nevents in each region
  hA = TH2D("hA", "hA", xbins, xmin, xcut, ybins, ycut, ymax)
  hA.Sumw2()
  hB = TH2D("hB", "hB", xbins, xcut, xmax, ybins, ycut, ymax)
  hB.Sumw2()
  hC = TH2D("hC", "hC", xbins, xcut, xmax, ybins, ymin, ycut)
  hC.Sumw2()
  hD = TH2D("hD", "hD", xbins, xmin, xcut, ybins, ymin, ycut)
  hD.Sumw2()
  h = TH2D("h_"+infile, "LJjet1_isoID*0.001:LJjet1_DPJtagger ", xbins, xmin, xmax, ybins, ymin, ymax)
  h.Sumw2()
  print("ymin, ycut, ymax: ", ymin, ycut, ymax)
  print("xmin, xcut, xmax: ", xmin, xcut, xmax)
  
  # define regional cuts
  cutA = "(LJjet1_isoID*0.001>="+str(xmin)+"&&LJjet1_isoID*0.001<"+str(xcut)+"&&LJjet1_DPJtagger>="+str(ycut)+"&&LJjet1_DPJtagger<="+str(ymax)+")"
  cutB = "(LJjet1_isoID*0.001>="+str(xcut)+"&&LJjet1_isoID*0.001<"+str(xmax)+"&&LJjet1_DPJtagger>="+str(ycut)+"&&LJjet1_DPJtagger<="+str(ymax)+")"
  cutC = "(LJjet1_isoID*0.001>="+str(xcut)+"&&LJjet1_isoID*0.001<"+str(xmax)+"&&LJjet1_DPJtagger>="+str(ymin)+"&&LJjet1_DPJtagger<"+str(ycut)+")"
  cutD = "(LJjet1_isoID*0.001>="+str(xmin)+"&&LJjet1_isoID*0.001<"+str(xcut)+"&&LJjet1_DPJtagger>="+str(ymin)+"&&LJjet1_DPJtagger<"+str(ycut)+")"
  cutABCD = "(LJjet1_isoID*0.001>="+str(xmin)+"&&LJjet1_isoID*0.001<="+str(xmax)+"&&LJjet1_DPJtagger>="+str(ymin)+"&&LJjet1_DPJtagger<="+str(ymax)+")"

  # errors for TH1 integral
  errA = ctypes.c_double(0)
  errB = ctypes.c_double(0)
  errC = ctypes.c_double(0)
  errD = ctypes.c_double(0)
  errN = ctypes.c_double(0)

  # create histogram and extract nevents for each region ABCD
  canvas = TCanvas("c", "c", 10, 10, 800, 500)
  canvas.cd()
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>hA", (presel+"*"+cutA+")"), "colz")
  nA = hA.IntegralAndError(0, xbins, 0, ybins, errA)
  
  canvas.Clear()
  canvas.cd()
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>hB", (presel+"*"+cutB+")"), "colz")
  nB = hB.IntegralAndError(0, xbins, 0, ybins, errB) 
  canvas.Clear()
  
  canvas.cd()
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>hC", (presel+"*"+cutC+")"), "colz")
  nC = hC.IntegralAndError(0, xbins, 0, ybins, errC)
  canvas.Clear()

  canvas.cd()
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>hD", (presel+"*"+cutD+")"), "colz")
  nD = hD.IntegralAndError(0, xbins, 0, ybins, errD)
  canvas.Clear()

  canvas.cd()
  
  if "QCD" in infile: title = "\\mbox{QCD: hDPJ isoID vs. hDPJ Tagger Score}"
  if "Data" in infile: title = "\\mbox{Full Run 2 Data: hDPJ isoID vs. hDPJ Tagger Score}"
  if "vbf" in infile: title = "\\mbox{"+str(infile.replace("frvz_", "").replace("_", " "))+": hDPJ isoID vs. hDPJ Tagger Score}"
 
  # draw histogram of total ABCD plane and save to file
  ttree.Draw("LJjet1_DPJtagger:LJjet1_isoID*0.001>>h_"+infile, (presel+"*"+cutABCD+")"), "colz")
  nh = h.IntegralAndError(0, xbins, 0, ybins, errN)
  n = h.IntegralAndError(0, xbins+1, 0, ybins+1, errN)
  gPad.Update()
  h.SetNameTitle(infile, title)
  h.SetXTitle("\mathrm{cDPJ\;isoID\;(GeV)}")
  h.SetYTitle("\mathrm{cDPJ\;Tagger\;Score}")
  h.GetXaxis().SetRange(0, xbins)
  h.GetYaxis().SetRange(0, ybins)
  h.GetYaxis().SetRangeUser(ymin, ymax)
  h.GetXaxis().SetRangeUser(xmin, xmax)
  print("without overflow", nh)
  print("with overflow", n)
  stats = h.FindObject("stats")
  stats.SetX1NDC(0.65)
  stats.SetX2NDC(0.85)
  stats.SetY1NDC(0.8)
  stats.SetY2NDC(0.6)
  #gStyle.SetOptStat(0)
  corr = round(h.GetCorrelationFactor(1, 2), 4)
  if mode == "correlation": print("ABCD plane correlation: " + str(corr))
  # BC sub-region validation
  if ymax == 0.95 and mode == "sub-region_validation":
    BC1 = TPaveText(xcut-2,ycut-0.5,xcut-0.5,ycut-0.2)
    BC2 = TPaveText(xcut-2,ycut+0.2,xcut-0.5,ycut+0.5)
    BC3 = TPaveText(xcut+0.5,ycut+0.2,xcut+2,ycut+0.5)
    BC4 = TPaveText(xcut+0.5,ycut-0.5,xcut+2,ycut-0.2)
    BC1.SetBorderSize(0)
    BC2.SetBorderSize(0)
    BC3.SetBorderSize(0)
    BC4.SetBorderSize(0)
    BC1.SetTextSize(0.06)
    BC2.SetTextSize(0.06)
    BC3.SetTextSize(0.06)
    BC4.SetTextSize(0.06)
    BC1.SetFillStyle(0)
    BC2.SetFillStyle(0)
    BC3.SetFillStyle(0)
    BC4.SetFillStyle(0)
    BC1.SetTextAlign(12)
    BC2.SetTextAlign(12)
    BC3.SetTextAlign(12)
    BC4.SetTextAlign(12)
    BC1.AddText("BC1");
    BC2.AddText("BC2");
    BC3.AddText("BC3");
    BC4.AddText("BC4");
    BC1.Draw()
    BC2.Draw()
    BC3.Draw()
    BC4.Draw()
  elif "control" in mode or mode == "estimation":
    A = TPaveText(xcut-0.75,ycut+0.01,xcut-0.25,ycut+0.04)
    B = TPaveText(xcut+0.25,ycut+0.01,xcut+0.75,ycut+0.04)
    C = TPaveText(xcut+0.25,ycut-0.04,xcut+0.75,ycut-0.01)
    D = TPaveText(xcut-0.75,ycut-0.04,xcut-0.25,ycut-0.01)
    A.SetBorderSize(0)
    B.SetBorderSize(0)
    C.SetBorderSize(0)
    D.SetBorderSize(0)
    A.SetTextSize(0.05)
    B.SetTextSize(0.05)
    C.SetTextSize(0.05)
    D.SetTextSize(0.05)
    A.SetFillStyle(0)
    B.SetFillStyle(0)
    C.SetFillStyle(0)
    D.SetFillStyle(0)
    A.SetTextAlign(12)
    B.SetTextAlign(12)
    C.SetTextAlign(12)
    D.SetTextAlign(12)
    if "control" in mode:
      A.AddText("A'");
      B.AddText("B'");
      C.AddText("C'");
      D.AddText("D'");
    else:
      A.AddText("A");
      B.AddText("B");
      C.AddText("C");
      D.AddText("D");
    A.Draw();
    B.Draw();
    C.Draw();
    D.Draw();
  if mode == "correlation":
    E = TPaveText(12,1.4,17,1.8)
    E.SetBorderSize(1)
    E.SetTextSize(0.05)
    E.SetTextColor(2)
    E.SetTextAlign(12)
    E.AddText(str(round(corr*100, 2)) + "% correlation");
    E.Draw();
  xcut = TLine(xcut, ymin, xcut, ymax)
  xcut.SetLineColor(kRed)
  xcut.SetLineWidth(2)
  ycut = TLine(xmin, ycut, xmax, ycut)
  ycut.SetLineColor(kRed)
  ycut.SetLineWidth(2)
  if "correlation" not in mode:
    xcut.Draw("same")
    ycut.Draw("same")
  canvas.SetLogz()
  canvas.Print("/Users/s1891044/Documents/Physics/DarkPhotons/vbf-hdpj-abcd-framework/output/dpjtagger_isoID/" + mode + "/" + infile + ".png")
  print("Plot saved to output/dpjtagger_isoID/" + mode + "/" + infile + ".png")
  # return nevents in each region + stat. error
  vals = [nA, nB, nC, nD, n, errA.value, errB.value, errC.value, errD.value, errN.value]
  print("nA: ", str(round(nA, 1)))
  print("nB: ", str(round(nB, 1)))
  print("nC: ", str(round(nC, 1)))
  print("nD: ", str(round(nD, 1)))
  if "Data" not in infile: print("n: ", round(n, 1), "±", round(errN.value, 1))
  canvas.Close()
  return vals
