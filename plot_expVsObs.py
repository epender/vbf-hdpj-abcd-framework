import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
import run # ABCD framework runfile

outDir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/control-region_validation/"
#outDir = "/Users/s1891044/Documents/Physics/DarkPhotons/vbf-mudpj-abcd-framework/output/charge_isoID/sub-region_validation/"

def get_nAs(nAs):
  with open(outDir+"output_T.csv", "r") as inFile:
    reader = csv.reader(inFile, delimiter=",")
    count = 0
    for row in reader:
      if count == 1: nAs[0].append(row[-1]) # append observed
      if count == 5: nAs[1].append(row[-1]) # append expected
      count+=1
  inFile.close()
  return nAs

def isoID():
  nAs = [[],[]]
  print("\n")
  print("Running over leading muDPJ isoID values...")
  cuts = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5]
  #cuts = [1,]
  for cut in cuts:
    print("Current leading muDPJ cut at " + str(cut))
    print("\n")
    run.main("control-region_validation", [cut, 1])
    #run.main("sub-region_validation", [cut, 2])
    nAs = get_nAs("LJmu1 isoID", nAs)
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, nAs, sliding_var="LJmu1_isoID")

def centrality():
  nAs = [[],[]]
  print("\n")
  print("Running over LJmu1 centrality values...")
  cuts = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
  #cuts = [0.4]
  for cut in cuts:
    print("Current LJmu1 centrality cut at " + str(cut))
    print("\n")
    run.main("control-region_validation", [4.5, cut])
    nAs = get_nAs("LJmu1 Centrality", nAs)
    print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, nAs, sliding_var="LJmu1_centrality")

def dphijj():
  nAs = [[],[]]
  print("\n")
  print("Running over |dphijj| values...")
  cuts = [2.5, 2.6, 2.7, 2.8, 2.9, 3.0]
  for cut in cuts:
    print("Current |dphijj| centrality cut at " + str(cut))
    print("\n")
    run.main("control-region_validation", [2, cut])
    nAs = get_nAs("|dphijj|", nAs)
    print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, nAs, sliding_var="|dphijj|")

def charge():
  xcut = 1
  nAs = [[],[]]
  print("\n")
  print("Running over muDPJ |charge| values...")
  cuts = [1, 2, 3, 4]
  for ycut in cuts:
    print("Current muDPJ |charge| cut at " + str(ycut))
    print("\n")
    run.main("control-region_validation", [xcut, ycut])
    nAs = get_nAs(nAs)
    print("nAs:" + str(nAs))
  print("nA_obs: " + str(nAs[0]))
  print("nA_exp: " + str(nAs[1]))
  print("\n")
  plot(cuts, xcut, nAs, sliding_var="muDPJ_charge")

def plot(cuts, static_cut, nAs, sliding_var):
  print("Now plotting...")
  print("\n")
  obs_nAs = [float(nA) for nA in nAs[0]] # no error on observed nA!
  obs_errs = [np.sqrt(obs_nA) for obs_nA in obs_nAs]
  obs_errsUp = [(obs_nAs[i] - obs_errs[i]) for i in range(len(obs_nAs))]
  obs_errsDown = [(obs_nAs[i] + obs_errs[i]) for i in range(len(obs_nAs))]
  exp_nAs = [float(exp_nA.split("±")[0]) for exp_nA in nAs[1]]
  exp_errs = [float(exp_nA.split("±")[1]) for exp_nA in nAs[1]]
  exp_errsUp = [(exp_nAs[i] - exp_errs[i]) for i in range(len(exp_nAs))]
  exp_errsDown = [(exp_nAs[i] + exp_errs[i]) for i in range(len(exp_nAs))]
  fig, ax = plt.subplots()
  ax = plt.plot(cuts, obs_nAs, label="Observed nA") # plot observed
  ax = plt.plot(cuts, exp_nAs, label="Expected nA") # plot expected
  plt.text(cuts[0], cuts[-1]-2, 'muDPJ isoID cut: '+str(static_cut)+'GeV', fontsize=15, color='red')
  plt.fill_between(cuts, exp_errsUp, exp_errsDown, color="orange", alpha=0.2) 
  plt.fill_between(cuts, obs_errsUp, obs_errsDown, color="blue", alpha=0.2) 
  plt.xlabel("Cut on " + sliding_var)
  plt.ylabel("Run 2 Data Observed/Expected in nA")

  plt.legend()
  #plt.title("ABCD Validation: BC Sub-Region \n Observed/Expected value of nA for Sliding Cuts on " + sliding_var)
  #plt.title("ABCD Validation: Inverted LJmuon_DNNscore \n Observed/Expected value of nA for Sliding Cuts on " + sliding_var)
  #plt.title("ABCD Validation: Inverted |dphijj| (no muDPJ Centrality cut) \n Observed/Expected value of nA for Sliding Cuts on " + sliding_var)
  plt.title("ABCD Validation: Inverted muDPJ Centrality (no |dphijj| cut) \n Observed/Expected value of nA for Sliding Cuts on " + sliding_var)
  plt.legend()
  loc = "obsVsExp_nA_"+sliding_var+".png"
  plt.savefig(outDir+loc)
  #plt.show()
  print("------------------------------------------------------")
  print("Finished - plot saved to control-region_validation/"+loc)
  print("------------------------------------------------------")
  plt.clf()

charge()
#isoID()
#centrality()
#dphijj()
