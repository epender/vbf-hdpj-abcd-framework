# This script runs after the VBF tagger dresser
# Adds LJmu1 quality cuts with inverted LJmuon_CNNscore   
# Branch required to form QCD enriched region with negligible signal
# For VBF muDPJ ABCD validation

from ROOT import TFile, TTree, TH1D, TLorentzVector
import sys, os, math
import numpy as np
import multiprocessing as mp
from array import array

def setup():

    inputDir = "/Users/s1891044/Documents/Physics/DarkPhotons/source/v02-00/"
    if not os.path.isdir(inputDir):
        print(inputDir + " does not exist, exitting")
        exit()
    # specifiy output
    outputDir = "/Users/s1891044/Documents/Physics/DarkPhotons/source/v02-00/abcd-samples/"
    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)
    print("output directory set to be " + outputDir)
    tree = "miniT" 

    # access each root file in the inputDir
    # apply cuts on the tree and save the output in the outputDir
    files = []
    outputfiles = []
    for file in os.listdir(inputDir):
        if 'ewk' in file or 'top' in file:
            files.append(inputDir + file)
            outputfiles.append(outputDir + file)
    pool = mp.Pool(len(files))
    results = [pool.apply_async(vbfdress, (files[i], outputfiles[i], tree, i)) for i in range(len(files))]
    pool.close()
    pool.join()


def vbfdress(file, outputname, tree, index):
    print("Processing", file, tree, outputname)
    tfile = TFile(file)
    ttree = tfile.Get(tree)
    if not ttree:
        print("no valid input tree, exitting")
        return 
    histos = [key.GetName() for key in tfile.GetListOfKeys() if key.GetClassName()[:2]=="TH"]
    tfile_out = TFile(outputname, "recreate")
    for x in histos:
        tfile_out.WriteTObject(tfile.Get(x))
    ttree_out = ttree.CloneTree(0)
    ttree_out.SetDirectory(tfile_out)
    
    m_LJmu1_isCR = array("i",[0])
    m_LJmu1_mu1DNNscore = array("d",[0])
    m_LJmu1_centrality = array("d",[0])
    m_LJmu1_charge = array("i",[0])
    m_LJjet1_centrality = array("d",[0])
    m_LJjet1_charge = array("i",[0])

    LJmu1_isCR_branch = ttree_out.Branch( 'LJmu1_isCR', m_LJmu1_isCR, 'LJmu1_isCR/I' )
    LJmu1_mu1DNNscore_branch = ttree_out.Branch( 'LJmu1_mu1DNNscore', m_LJmu1_mu1DNNscore, 'LJmu1_mu1DNNscore/D' )
    LJmu1_centrality_branch = ttree_out.Branch( 'LJmu1_centrality', m_LJmu1_centrality, 'LJmu1_centrality/D' )
    LJmu1_charge_branch = ttree_out.Branch( 'LJmu1_charge', m_LJmu1_charge, 'LJmu1_charge/I' )
    LJjet1_centrality_branch = ttree_out.Branch( 'LJjet1_centrality', m_LJjet1_centrality, 'LJjet1_centrality/D' )
    LJjet1_charge_branch = ttree_out.Branch( 'LJjet1_charge', m_LJjet1_charge, 'LJjet1_charge/I' )
    
    sumOfWeights_nEvents = tfile.numEvents.GetBinContent(1)
    # this is the default sum of weight 
    sumOfWeights = tfile.numEvents.GetBinContent(2)
    for entry in range(ttree.GetEntries()):
        ttree.GetEntry(entry)
        #if entry > 10 :  continue
        if entry > 0 and entry%100000==0:
            print("Processed {} of {} entries".format(entry,ttree.GetEntries()))
        
        m_LJmu1_isCR[0] = 1
        m_LJmu1_mu1DNNscore[0] = -1
        m_LJmu1_centrality[0] = -1
        m_LJmu1_charge[0] = -99
        m_LJjet1_centrality[0] = -1
        m_LJjet1_charge[0] = -99
        
        LJmus20 = []
        LJjets20 = []
        # for centrality calculation
        square_dj = (ttree.jet1_eta - ttree.jet2_eta)**2
        half_jet_sum = (ttree.jet1_eta + ttree.jet2_eta)/2
        for idx_LJ in range(ttree.ptLJ.size()): 
            if ttree.ptLJ.at(idx_LJ) > 20e3 and ttree.types.at(idx_LJ) == 0: LJmus20.append(idx_LJ)
            if ttree.ptLJ.at(idx_LJ) > 20e3 and ttree.types.at(idx_LJ) == 2: LJjets20.append(idx_LJ)
        ttree_out.GetEntry(entry)
        xs = ttree.amiXsection
        weight = ttree.weight
        # Apply additional LJmu1 quality cuts
        if len(LJmus20) > 0:
            # find leading muonic DPJ centrality (wrt. vbf jets) 
            LJmu1_centrality = np.exp(-(4/square_dj)*((ttree.LJmu1_eta - half_jet_sum)**2))
            m_LJmu1_centrality[0] = LJmu1_centrality
            # find leading LJmu index
            idx_LJmu1 = LJmus20[0]
            for idx_LJ in range(len(ttree.ptLJ)): 
                if ttree.ptLJ.at(idx_LJ) > ttree.ptLJ.at(idx_LJmu1): idx_LJmu1 = idx_LJ
            LJmuons_charge = []
            # access muons associated with the leading muon DPJ
            for mu_idx in range(ttree.LJmuon_index.size()):
                if ttree.LJmuon_index.at(mu_idx) == idx_LJmu1:
                    LJmuons_charge.append(ttree.LJmuon_charge.at(mu_idx))
                    LJmu_eta = abs(ttree.LJmuon_eta.at(mu_idx))
                    mu1_DNNscore = ttree.LJmuon_DNNscore.at(mu_idx)
                    m_LJmu1_mu1DNNscore[0] = mu1_DNNscore
                    if LJmu_eta > 1.0 and LJmu_eta < 1.1: m_LJmu1_isCR[0] = 0
                    # Does LJmu1 pass quality cuts? 
                    if ttree.LJmuon_author.at(mu_idx) == 1 or (mu1_DNNscore < 0.2 or mu1_DNNscore > 0.5): m_LJmu1_isCR[0] = 0
            # sum of charge of leading muDPJ constituents
            m_LJmu1_charge[0] = abs(sum(LJmuons_charge))
        
        # Apply additional LJjet1 quality cuts
        if len(LJjets20) > 0:
            # find leading calo DPJ centrality (wrt. vbf jets) 
            LJjet1_centrality = np.exp(-(4/square_dj)*((ttree.LJjet1_eta - half_jet_sum)**2))
            m_LJjet1_centrality[0] = LJjet1_centrality
            # LJjet1 charge does not exist!
            '''idx_LJjet1 = LJjets20[0]
            for idx_LJjet in range(len(ttree.ptLJ)): 
                if ttree.ptLJ.at(idx_LJjet) > ttree.ptLJ.at(idx_LJjet1): idx_LJjet1 = idx_LJjet
            LJjets_charge = []
            # access jets associated with the leading calo DPJ
            for jet_idx in range(ttree.LJjet_index.size()):
                if ttree.LJjet_index.at(jet_idx) == idx_LJjet1: 
                  LJjets_charge.append(ttree.LJjet_charge.at(jet_idx))
            # sum of charge of leading calo DPJ constituents
            m_LJjet1_charge[0] = abs(sum(LJjets_charge))'''
        ttree_out.Fill()
    tfile.Close()
    
    print ("")
    print ("SAMPLE " + file.rsplit("/", 1)[1])
    print ("SumOfWeights: " + str(sumOfWeights))
    tfile_out.Write()
    tfile_out.Close()

if __name__=='__main__':
    setup()            
